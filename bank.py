#!/usr/bin/env python
# -*- coding: utf-8 -*-

import queue
import account
import threading


class Bank():
    def __init__(self):
        self.__accounts = {}
        self.__last_id = 0

    def inc_last_id(self):
        self.__last_id += 1

    def add_account(self, name, funds):
        assert(len(name) > 0 and funds >= 0)
        self.__accounts[self.__last_id] = account.Account(name)
        self.change_account_funds(self.__last_id, funds)
        self.inc_last_id()

    def remove_account(self, acc_id):
        assert(acc_id in self.__accounts)
        del self.__accounts[acc_id]

    def get_account(self, acc_id):
        assert(acc_id in self.__accounts)
        return self.__accounts[acc_id]

    def change_account_owner(self, acc_id, new_name):
        assert(acc_id in self.__accounts)
        self.__accounts[acc_id].change_owner(new_name)

    def change_account_funds(self, acc_id, amount):
        assert(acc_id in self.__accounts)
        self.__accounts[acc_id].change_funds(amount)

    def make_transaction(self, payer_id, receiver_id, amount):
        assert(payer_id in self.__accounts and receiver_id in self.__accounts)
        assert(self.__accounts[payer_id].get_funds() >= amount)
        self.change_account_funds(payer_id, -amount)
        self.change_account_funds(receiver_id, amount)

    def print_accounts(self):
        print("Printing out all stored accounts")
        for v in self.__accounts.values():
            print("{} {}".format(v.get_name(), v.get_funds()))

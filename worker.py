#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import time
import random


class Worker(threading.Thread):
    def __init__(self, thread_id, work_queue, bank, lock):
        threading.Thread.__init__(self)
        self.__thread_id = thread_id
        self.__lock = lock
        self.__work_queue = work_queue
        self.__bank = bank

    def run(self):
        while True:
            try:
                f, t, amount = self.__work_queue.get(True)
                time.sleep(random.uniform(1, 10))
                with self.__lock:
                    print("Sending {}eur from {} to {}".format(amount, f, t))
                    self.__bank.make_transaction(f, t, amount)
                    self.__bank.print_accounts()
            finally:
                self.__work_queue.task_done()

#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Account:
    def __init__(self, name):
        self.__name = name
        self.__funds = 0

    def change_owner(self, new_name):
        self.__name = new_name

    def change_funds(self, amount):
        if amount < 0:
            assert(self.__funds >= amount)
        self.__funds += amount

    def get_name(self):
        return self.__name

    def get_funds(self):
        return self.__funds

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bank
import queue
import threading
import worker


def example():
    b = bank.Bank()
    b.add_account("jano", 10)
    b.add_account("jozo", 20)
    b.add_account("fero", 30)
    b.add_account("juro", 40)
    b.add_account("peto", 50)

    b.print_accounts()

    print("Insert number_of_clients, number_of_requests")
    number_of_clients, number_of_requests = map(int, input().split())

    work_queue = queue.Queue()
    lock = threading.Lock()

    for i in range(number_of_clients):
        w = worker.Worker(i, work_queue, b, lock)
        w.daemon = True
        w.start()

    for i in range(number_of_requests):
        print("Insert from, to, amount")
        f, t, a = map(int, input().split())
        work_queue.put((f, t, a))
    print("End of input")

    work_queue.join()


if __name__ == '__main__':
    example()
